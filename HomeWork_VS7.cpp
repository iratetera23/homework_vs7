

#include <iostream>

class Animal
{	
public:
	virtual void Voice()
	{
		std::cout << "Noise" << '\n';
	}
};

class Parrot : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Parrot says: Rum! Rum! Rum!" << '\n';
	}
};

class Wolf : public Animal
{
public:
	void Voice() override
	{
		setlocale(LC_ALL, "Russian");
		std::cout << "Wolf says: ����� ���� ��������� � ������, ��� ������ � ���������." << '\n';
	}
};

class Frog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Frog says: It is Wednesday, my dudes." << '\n';
	}
};

int main()
{
	Animal* animals[3];
	animals[0] = new Parrot();
	animals[1] = new Wolf();
	animals[2] = new Frog();

	for (int i = 0; i < 3; i++) 
	{
		animals[i]->Voice();
	}

	return 0;
  
}